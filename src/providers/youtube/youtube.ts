import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {YouTubeAPIRequest} from "../../interfaces/youtube.interfaces";
import {map} from "rxjs/operators";

const API_KEY = 'AIzaSyDSHur7cIhQ63t0wLut-KnHS2ppJIwvWSw';
const API_URL = 'https://www.googleapis.com/youtube/v3/';
const REGION_CODE = 'FR';

@Injectable()
export class YoutubeProvider {

  constructor(public http: HttpClient) {
    console.log('Hello YoutubeProvider Provider');
  }

  public recupereCategories(): Observable<YouTubeAPIRequest> {
    return this.http
        .get(API_URL + 'videoCategories?part=snippet&regionCode=' + REGION_CODE + '&key=' + API_KEY)
        .pipe(
            map((response : YouTubeAPIRequest) => { return response; })
        )

  }

  public searchVideos(categoryId: number = 1): Observable<YouTubeAPIRequest>{
      return this.http
          .get(API_URL + 'search?part=snippet&videoCategoryId=' + categoryId + '&key=' + API_KEY + '&maxResults=10&type=video')
          .pipe(
              map((response : YouTubeAPIRequest) => { return response; })
          )

  }

    public searchTrandingVideos(): Observable<YouTubeAPIRequest> {
        return this.http.get(`${API_URL}videos?part=snippet&chart=mostPopular&key=${API_KEY}&regionCode=${REGION_CODE}&maxResults=10`)
            .pipe(
                map((response : YouTubeAPIRequest) => { return response; })
            )
    }

}
