import { Component } from '@angular/core';
import {Loading, LoadingController, NavController} from 'ionic-angular';
import {YoutubeProvider} from "../../providers/youtube/youtube";
import {Items} from "../../interfaces/youtube.interfaces";
import {YoutubeVideoPlayer} from "@ionic-native/youtube-video-player";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  categories : Items[] = [];
  videos : Items[] = [];
  loader: Loading;

  constructor(public navCtrl: NavController, private youtubeProvider: YoutubeProvider, private loadingCtrl : LoadingController, public youtube: YoutubeVideoPlayer) {
    this.youtubeProvider.recupereCategories().subscribe(data => {
      if (data){
          this.categories = data.items;
      }
    })


      this.searchTrandingVideos();

  }

  public searchVideos(categoryId : number) {
      this.presentLoading();
    this.youtubeProvider.searchVideos(categoryId).subscribe(data => {
        if (data){
            this.videos = data.items;
            this.loader.dismiss();
        }

    })
  }

    public searchTrandingVideos() {
      this.presentLoading();
        this.youtubeProvider.searchTrandingVideos().subscribe(data => {
            if (data){
                this.videos = data.items;
                this.loader.dismiss();
            }

        })
    }

    private presentLoading() {
        this.loader = this.loadingCtrl.create({
            content: "La cavalerie arrive ..."
        });
        this.loader.present();
    }

}
